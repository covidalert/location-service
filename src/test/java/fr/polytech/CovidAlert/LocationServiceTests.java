package fr.polytech.CovidAlert;


import fr.polytech.CovidAlert.models.Location;
import fr.polytech.CovidAlert.repositories.LocationRepository;
import fr.polytech.CovidAlert.services.LocationService;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.sql.Timestamp;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.mockito.Mockito.*;

@WebMvcTest(LocationService.class)
public class LocationServiceTests {
    @Autowired
    private LocationService locationService;

    @MockBean
    private LocationRepository repository;

    @Test
    @DisplayName("get - Success")
    public void getSuccess() {
        Location mockLocation = new Location(1, 13.0, 12.0,  new Timestamp(1000000), "a34kAo90");
        doReturn(Optional.of(mockLocation)).when(repository).findById(1L);

        Optional<Location> foundTest = locationService.get(1L);

        Assertions.assertTrue(foundTest.isPresent(), "Location was not found");
        Assertions.assertSame(foundTest.get(), mockLocation, "Locations should be the same");
    }

    @Test
    @DisplayName("get - Not found")
    public void getNotFound() {
        doReturn(Optional.empty()).when(repository).findById(1L);

        Optional<Location> foundTest = locationService.get(1L);

        Assertions.assertFalse(foundTest.isPresent(), "Location was found when it should not be");
    }

    @Test
    @DisplayName("create - Success")
    public void createSuccess() {
        Location mockLocation = new Location(1, 13.0, 12.0,  new Timestamp(1000000), "a34kAo90");
        doReturn(mockLocation).when(repository).saveAndFlush(mockLocation);

        Location createdTest = locationService.create(mockLocation);

        Assertions.assertSame(createdTest, mockLocation, "Incoming Location and created Location should be the same");
    }

    @Test
    @DisplayName("delete - Success")
    public void deleteSuccess() {
        Location mockLocation = new Location(1, 13.0, 12.0,  new Timestamp(1000000), "a34kAo90");
        doNothing().when(repository).deleteById(1L);
    }

    @Test
    @DisplayName("delete - Not found")
    public void deleteNotFound() {
        Location mockLocation = new Location(1, 13.0, 12.0,  new Timestamp(1000000), "a34kAo90");
        doReturn(false).when(repository).existsById(1L);

        Assertions.assertThrows(NoSuchElementException.class, () -> locationService.delete(1L));
    }
}
